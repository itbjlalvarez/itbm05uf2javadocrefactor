package com.company;

import com.company.calculadora.CalculadoraUI;

import java.util.Scanner;

/**
 *
 * Classe que implementa el programa principal pensat per fer fer servir vàries utilitats. Actualment només permet fer servir una:
 * una calculadora simple am interfase d'usuari en format d'un menú de text.
 * Veure la classe <code>CalculadoraUI</code> per saber quines operacions estàn implementades actualment.
 *
 * El programa accepta un paràmetre no obligatori que indica si l'usuari pot accedir a totes les operacions disponibles a la calculadora. Veure la
 * descripció del métode <code>main</code> per més detalls.
 *
 * @author José Luis Álvarez Casas
 * @version v1.0 (18/03/2021)
 * @see #main(String[])
 */
public class Aplicacio {

    /**
     * Métode principal que mostra un menú en format de text, el qual actualment només mostra les següents opcions:
     *
     *  1 - Calculadora
     *  2 - opció 2 (no implementada)
     *  3 - Sortir (de l'aplicació)
     *
     *  La calculdora està implementada per la classe <code>CalculadoraUI</code>, a la qual se li passa un paràmetre,
     *  <code>mostraTot</code> que indica si es vol tenir accés a totes les operacions que permet la calculadora.
     *
     *  Si la classe <code>Aplicacio</code> és executada amb un paràmetre de valor igual a 1, aleshores <code>mostraTot</code>
     *  tindrà el valor <code>true</code>, en cas contrari (o si no es passa el paràmetre, ja que aquest no és obligatori), el
     *  valor de <code>mostraTot</code> serà <code>false</code>.
     * @param args  si val "1" la calculadora mostrarà totes les operacions disponibles. En cas contrari, o si no té cap
     *             valor aleshores la calculadora no les mostrara totes.
     *
     * @see com.company.calculadora.CalculadoraUI
     * @see com.company.calculadora.CalculadoraUI#show(boolean)
     */
    public static void main(String[] args) {
        boolean mostraTot = false;

        if (args.length > 0) {
            mostraTot = args[0].equals("1");
        }

        Scanner input = new Scanner(System.in);

        int opcio = mostraMenu();

        while (opcio != 3) {

            switch (opcio) {

                case 1:
                    CalculadoraUI calculadoraUI = new CalculadoraUI();
                    calculadoraUI.show(mostraTot);
                    break;
                case 2:
                    System.out.println("opcio = 2");
                    break;
                default:
                    System.out.println("Opció no comtemplada, si us plau entreu una opcio entre [1-3]");
            }

            opcio = mostraMenu();
        }
        System.out.println("Sortint del programa. Adéu!");

    }

    /**
     * Métode que mostra el menú d'opcions de l'aplicació en format de text.
     * @return opcio valor numéric introduït per l'usuari
     */
    public static int mostraMenu() {

        int opcio;
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Menú:");
        System.out.println("--------------");
        System.out.println("1. Calculadora");
        System.out.println("2. opció 2");
        System.out.println("3. Sortir");
        System.out.println("--------------");
        System.out.println("Entra la opció:");
        opcio = keyboard.nextInt();

        return opcio;

    }
}
