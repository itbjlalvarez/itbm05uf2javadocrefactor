package com.company;

import java.util.Random;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * Classe que implementa el joc de que l'usuari intenti adivinar un número enter entre 0 i 99 generat de manera pseudoaleatòria.
 * Actualment el programa no para fins que l'usuari endevina el número. Quan finalment ho fa, mostra en pantalla el número d'intents.
 * Segurament caldria ampliar el programa per tal de que es fixi o es pregunti a l'usuari el número màxim d'intents i també el rang
 * d'entre els quals el número s'ha de generar (entre 0 i N, o inclús entre A i B)
 *
 * El programa no espera trobar cap paràmetre i per tant ignora qualsevol paràmetre passat a la seva execució.
 *
 * @author José Luis Álvarez Casas
 * @version v1.0 (18/03/2021)
 * @see #main(String[])
 */
public class AdivinaNumero {

    /**
     * Métode principal. Genera un número aleatori entre 0 i 99 i pregunta a l'usuari quin número creu que s'ha generat.
     * El métode no acaba fins que l'usuari ho encerta. Al finalitzar mostra el número d'intents que han sigut
     * necessaris per endevinar el número.
     * 
     * @param args s'ignora
     * @see #input(String)
     */
    public static void main(String[] args) {
        Random r = new Random();
        int adivinar = r.nextInt(100); // genera un número entre 0 y 99
        int intentos = 0; // crea una variable para contar los intentos fallidos
        int apuesta = input("Adivina el número secreto entre 0 y 99.");
        while (adivinar != apuesta) { // si el número dado es distinto al sorteado repite
            intentos = intentos + 1; // incrementa variable contador
            if (adivinar > apuesta) {
                apuesta = input("El número a adivinar es más grande. \n Inténtalo otra vez");
            } else {
                apuesta = input("El número a adivinar es más pequeño. \n Inténtalo otra vez");
            }
        }
        System.out.println("Felicidades adivinaste el número tras " + (intentos + 1) + " intentos!");

    }

    /**
     * Métode que mostra en pantalla el text passat per paràmetre i sol·licita també per pantalla un número enter el
     * qual serà retornat pel métode.
     * @param texto text a mostrar en pantalla
     * @return número enter entrat per l'usuari
     */
    private static int input(String texto) {
        System.out.println(texto);
        Scanner keyboard = new Scanner(System.in);
        return keyboard.nextInt();
    }
}
