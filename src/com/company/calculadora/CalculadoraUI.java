package com.company.calculadora;

import java.util.Scanner;
import java.util.InputMismatchException;

/**
 *
 * Classe que implementa la interfase d'usuari en format d'un menú de text d'una calculadora i els métodes que implementa les operacions que aquesta pot fer.
 * A data d'avui, 18/03/2021, pot fer les següents funcionalitats:
 * - sumar
 * - restar
 * - multiplicar
 * - dividir
 * - mostrar el resultat de l'anterior operació
 *
 * Les operacions de multiplicar i divisió només estàn disponibles si s'invoca al métode principal de la classe, "show", amb el paràmetre "mostraTot" igual a <code>true</code>.
 *
 * @author José Luis Álvarez Casas
 * @version v1.0 (18/03/2021)
 * @see #show(boolean)
 */
public class CalculadoraUI {

    /**
     * Constant per indicar quina és l'opció per sortir del menú de text que implementa la interficie d'usuari de la calculadora.
     * @see #show(boolean)
     */
    private static final int EXIT_OPTION = 6;
    /**
     *  La propietat <code>scanner</code> serveix per a llegir de l'entrada estàndard (la "consola") quina opció del menú de la calculadora
     *  es vol executar i si és el cas, amb quins números es vol realitzar l'operació aritmética desitjada.
     * @see #CalculadoraUI
     * @see #show(boolean)
     */
    private final Scanner scanner;

    /**
     * Propietat que emmagatzema l'últim resultat realitzat per la calculadora.
     * @see #show(boolean)
     * @see #getLasResult()
     */
    private double lastResult = 0;

    /**
     * A banda de ser el constructor sense paràmetres de la classe, inicilitza la propietat <code>scanner</code>
     */
    public CalculadoraUI() {
        scanner = new Scanner(System.in);
    }

    /**
     * El métode mostra el menú de les operacions disponibles (més l'opció de sortir del propi menú) Un cop l'usuari especifica quina operació vol fer,  el métode demana els paràmetres
     * necessaris per portar-la a terme i invoca el métode que implementa l'operació.
     * El resultat de l'última operació s'emmagatzema a la propietat <code>lastResult</code>
     *
     * @param mostraTot si <code>true</code> permet fer servir les quatre operacions aritmètiques bàsiques. Si <code>false</code>, només permet fer servir la suma i la resta
     * @see #add(double, double)
     * @see #subtract(double, double)
     * @see #multiply(double, double)
     * @see #divide(double, double)
     * @see #getLasResult() 
     */
    public void show(boolean mostraTot) {
        boolean finish = false;
        System.out.println("-----------------------------------------------");
        System.out.println("-                                             -");
        System.out.println("-            Simple Calculadora                -");
        System.out.println("-                                             -");
        System.out.print("-----------------------------------------------");
        while (!finish) {
            try {
                // Muestra menú
                System.out.println();
                System.out.println("Main Menu: ");
                System.out.println("1. Add");
                System.out.println("2. Subtract");
                if (mostraTot) {
                    System.out.println("3. Multiply");
                    System.out.println("4. Divide");
                }
                System.out.println("5. Mostra el resultat de la última operació");
                System.out.println("6. Exit");
                System.out.println("-------------------------------------------------------");

                System.out.print("Select an option from the menu: ");
                // Read user selected option
                int opcio = scanner.nextInt();
                // Validate selected option
                if (opcio >= 1 && opcio <= EXIT_OPTION) {
                    if (opcio == EXIT_OPTION) {
                        finish = true;
                    } else {
                        // ejecuta operación
                        if (opcio != 5) {
                            System.out.print("Entra el primer número:");
                            int a = scanner.nextInt();
                            System.out.print("Entra el segon número:");
                            int b = scanner.nextInt();
                            switch (opcio) {
                                case 1:
                                    lastResult = add( a, b);
                                    break;
                                case 2:
                                    lastResult = subtract(a, b);
                                    break;
                                case 3:
                                    lastResult = multiply(a, b);
                                    break;
                                case 4:
                                    lastResult = divide(a, b);
                                    break;
                            }
                        }
                        // mostra resultat última operació
                        System.out.println("-------------------------------------------------------");
                        System.out.println(" ---> El resultat de la última operació és: " + getLasResult());
                        System.out.println("-------------------------------------------------------");
                    }
                } else {
                    System.out.println("Opció invalida. Mira't de nou el menú i pica una opció correcta.");
                }
            } catch (InputMismatchException ex) {
                scanner.nextLine();
                System.out.println("El valor que has entrat és incorrecte.");
            }
        }
    }

    /**
     * Implementa la suma de dos números de tipus <code>long</code>
     * @param a primer operand
     * @param b segon operand
     * @return el resultat de realitzar a+b
     */
    public double add(double a, double b){
        return a+b;
    }

    /**
     * Implementa la resta de dos números de tipus <code>long</code>
     *
     * @param a primer operand
     * @param b segon operand
     * @return el resultat de realitzar a-b
     */
    public double subtract(double a, double b){
        return a-b;
    }

    /**
     * Implementa la multiplicació de dos números de tipus <code>long</code>
     *
     * @param a primer operand
     * @param b segon operand
     * @return el resultat de realitzar a*b
     */
    public double multiply(double a, double b){
        return a*b;
    }

    /**
     * Implementa la divisió de dos números de tipus <code>long</code>
     *
     * @param a primer operand
     * @param b segon operand
     * @return el resultat de realitzar a/b
     */
    public double divide(double a, double b){
        return a/b;
    }

    /**
     * Permet consultar el valor de la propietat <code>lastResult</code>
     * @return el valor de <code>lastResult</code>
     * @see #lastResult
     */
    public double getLasResult(){
        return lastResult;
    }

}
